/**
 * 
 */
package stack_using_queues;

/**
 * @author nitkulsh
 *
 */
public class ImplClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CustomStack stack=new CustomStack();
		stack.push(9);
		stack.push(2);
		stack.push(3);
		System.out.println("MIN : "+stack.getMin());
		stack.pop();
		stack.push(1);
		System.out.println("MIN : "+stack.getMin());
		stack.pop();
		stack.printVal();
		

	}

}

package stack_using_queues;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class CustomStack {

	private Queue<Integer> queue1;
	private Queue<Integer> queue2;
	private Stack<Integer> suppStack;
	private Integer minVal;

	public CustomStack() {
		queue1 = new LinkedList<Integer>();
		queue2 = new LinkedList<Integer>();
		suppStack=new Stack<Integer>();
		minVal = Integer.MAX_VALUE;
	}

	public void push(Integer item) {
		queue1.offer(item);
		if (item <= minVal) {
			suppStack.push(item);
			this.minVal = suppStack.peek();
		}

	}

	public Integer pop() {
		Integer item = null;
		while (this.queue1.size() > 1) {
			queue2.offer(queue1.poll());
		}
		item = this.queue1.poll();
		queue1 = queue2;
		queue2 = new LinkedList<Integer>();
		if (item == this.minVal) {
			this.suppStack.pop();
			this.minVal = suppStack.peek();
		}
		return item;

	}

	public void printVal() {
		int size = queue1.size();
		for (int i = 0; i < size; i++) {
			System.out.println(queue1.poll() + " ");
		}
	}

	public Integer getMin() {
		return minVal;
	}

}

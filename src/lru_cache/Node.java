/**
 * 
 */
package lru_cache;

/**
 * @author nitkulsh
 *
 */
public class Node<K> {

	K val;
	Node prev;
	Node next;

	public Node(K val) {
		this.val=val;
		this.prev = null;
		this.next = null;
	}

}

package lru_cache;

public class CallerClass {
	
	public static void main(String[] args) {
		
		CustomLRUCache<Integer> lru=new CustomLRUCache<Integer>(4);
		lru.refer(1);
		lru.refer(2);
		lru.refer(3);
		lru.refer(4);
		lru.printCacheValues();
		lru.refer(2);
		lru.printCacheValues();
		lru.refer(9);
		lru.printCacheValues();
	}

}

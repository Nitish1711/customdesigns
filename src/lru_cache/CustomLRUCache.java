package lru_cache;

import java.util.HashMap;

// 1 2 4 6 
public class CustomLRUCache<K> {

	private Node head;
	HashMap<K, Node> holderMp;

	private int capacity;

	public CustomLRUCache(int capacity) {
		this.capacity = capacity;
		holderMp=new HashMap<K, Node>();
	}

	public void refer(K item) {
		if (holderMp.size() == this.capacity) {
			if(holderMp.containsKey(item)) {
				removeNodeFromList(holderMp.get(item));
			}else {
				head=head.next;
				holderMp.remove(item);
			}
		}
		addItemtoEnd(item);
	}

	private void addItemtoEnd(K item) {
		Node curr = head;
		Node newNode = new Node(item);
		holderMp.put(item, newNode);
		if(curr==null) {
			head=newNode;
			return;
		}
		while (curr.next != null) {
			curr = curr.next;
		}
		newNode.prev = curr;
		curr.next = newNode;
	}

	private void removeNodeFromList(Node node) {
		Node prev = node.prev;
		Node next = node.next;
		if (prev != null)
			prev.next = next;
		else
			node = node.next;
	}
	
	
	public void printCacheValues() {
		System.out.println("");
		Node curr=head;
		while(curr!=null) {
			System.out.print(curr.val+" ");
			curr=curr.next;
		}
	}
	
	

}

package customHashSet;

import java.util.HashMap;
import java.util.Map;

public class CustomHashSet<K> {

	private Map<K, Object> valueHoldermp;

	private Object DUMMY = new Object();

	public CustomHashSet() {
		valueHoldermp = new HashMap<K, Object>();
	}

	public boolean add(K item) {
		if (valueHoldermp.put(item, DUMMY) == null)
			return true;
		return false;
	}

	public void remove(K item) {
		valueHoldermp.remove(item);
	}
	
	public void print() {
		this.valueHoldermp.forEach((key,value)->{
			System.out.print(key+" ");
		});
	}

}

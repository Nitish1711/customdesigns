/**
 * 
 */
package producerConsumer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author nitkulsh
 *
 */
public class Supplier {

	private static int count = 0;

	public Supplier() {
	}

	public static void main(String[] args) throws InterruptedException {

		Processor processor = new Processor(5);

		Runnable producer = () -> {
			System.out.println("Going to produce items");
			try {
				processor.produce();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};

		Runnable consumer = () -> {
			System.out.println("Going to consume items");
			processor.consume();
		};

		Thread t1 = new Thread(producer);
		Thread t2 = new Thread(consumer);
		t1.start();
		t2.start();

	}

	static class Processor {
		private BlockingQueue<Integer> processQueue;
		private int capacity;

		public Processor(int capacity) {
			this.capacity = capacity;
			processQueue = new ArrayBlockingQueue<Integer>(capacity);
		}

		public void produce() throws InterruptedException {
			while (true) {
				synchronized (this) {
					try {
						if (processQueue.size() == 0) {
							System.out.println("Consumer waiting");
							wait();
						}
						processQueue.add(++count);

						notify();
						Thread.sleep(2000);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

		public void consume() {
			while (true) {
				synchronized (this) {
					try {
						if (processQueue.size() == capacity) {
							System.out.println("Producer waiting");
							wait();
						}
						processQueue.take();
						System.out.println("Producer notified ");
						notify();
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}

	}
}

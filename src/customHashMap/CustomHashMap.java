/**
 * 
 */
package customHashMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author nitkulsh
 *
 */
public class CustomHashMap<K, V> {

	Entry<K, V> tableHash[];
	private int capacity = 4;

	public CustomHashMap() {
		tableHash = new Entry[this.capacity];
	}

	public void put(K key, V value) {
		int index = calcHashCode(key);
		Entry<K, V> designatedBucket = tableHash[index];
		Entry<K, V> newEntry = new Entry<K, V>(key, value);

		if (designatedBucket != null) {
			Entry<K, V> curr = designatedBucket;
			Entry<K, V> prev = null;
			while (curr != null) {
				if (curr.key == key) {
					curr.value = value;
					return;
				}
				prev = curr;
				curr = curr.next;
			}
			prev.next = newEntry; // for the case when no match is found
			return;

		} else {
			tableHash[index] = newEntry; // if bucket is null in that case
			return;
		}
	}

	public V get(K key) {
		int index = calcHashCode(key);
		V value = null;
		Entry<K, V> eligibleBucket = tableHash[index];
		if (eligibleBucket != null) {
			Entry<K, V> curr = eligibleBucket;
			while (curr != null) {
				if (curr.key == key) {
					value = curr.value;
					return value;
				}
				curr = curr.next;
			}
		}
		return value;
	}

	public int calcHashCode(K key) {
 		return Math.abs(key.hashCode()) % 4;
		
	}

	public void printHashMap() {
		for (int i = 0; i < tableHash.length; i++) {
			Entry<K, V> curr = tableHash[i];
			while (curr != null) {
				System.out.println("Key : " + curr.key + "  value  :  " + curr.value);
				curr=curr.next;
			}
		}
	}

}

/**
 * 
 */
package customHashMap;

/**
 * @author nitkulsh
 *
 */
public class InvokerClass {
	
public static void main(String[] args) {
	
	CustomHashMap<String, Integer> empMap=new CustomHashMap<String, Integer>();
	empMap.put("Nitish", 1);
	empMap.put("Ashish", 2);
	empMap.put("Anurag", 3);
	empMap.printHashMap();
	empMap.put("Nitish", 5);
	empMap.printHashMap();
	System.out.println(empMap.get("Nitish"));


}
}

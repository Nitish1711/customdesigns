/**
 * 
 */
package queue_using_stacks;

/**
 * @author nitkulsh
 *
 */
public class ImplClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CustomQueue queue = new CustomQueue();
		queue.offer(1);
		queue.offer(2);
		queue.offer(3);
		System.out.println(queue.poll());
		System.out.println(queue.poll());
		queue.offer(9);
		System.out.println(queue.poll());
	}

}

package queue_using_stacks;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class CustomQueue {

	private Stack<Integer> stack1;
	private Stack<Integer> stack2;

	public CustomQueue() {
		stack1 = new Stack<Integer>();
		stack2 = new Stack<Integer>();
	}


	public void offer(Integer item) {
		stack1.push(item);
	}
	
	public Integer poll() {
		Integer item=null;
		if(stack2.isEmpty()) {
			while(!stack1.isEmpty()) {
				stack2.push(stack1.pop());
			}
		}
		item=stack2.pop();
		return item;
	}
}

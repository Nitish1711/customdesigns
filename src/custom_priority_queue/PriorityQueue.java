/**
 * 
 */
package custom_priority_queue;

/**
 * @author nitkulsh
 *
 */
public class PriorityQueue {

	private int[] heap = new int[10];
	
	private static int currHeapSize=0;

	public void add(Integer item) {
		int i = currHeapSize;
			while (i > 0 && item > heap[(i - 1) / 2]) {
				heap[i] = heap[(i - 1) / 2];
				i = (i - 1) / 2;
			}
			heap[i] = item;
			
			currHeapSize++;
	
	}

	public Integer poll() {
		int temp = heap[0];
		heap[0] = heap[currHeapSize-1];
		heap[currHeapSize-1]=0;
		int remainHeapSize =  currHeapSize-1;
		int i = 0;
		int child = 2 * i + 1; // default left child
		while (i <= remainHeapSize-1) {
			if (heap[child] < heap[child + 1]) {
				child = child + 1;
			}
			if (heap[child] > temp) {
				int tempVar = heap[child];
				heap[child] = heap[i];
				heap[i] = tempVar;
				i = child;
				child = 2 * i + 1;
			} else
				break;
		}

		return temp;

	}

}

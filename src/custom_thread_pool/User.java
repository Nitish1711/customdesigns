package custom_thread_pool;

public class User {

	public static void main(String[] args) throws InterruptedException {

		Runnable task1 = () -> {
			System.out.println(Thread.currentThread().getName() + " performing task1");
		};

		Runnable task2 = () -> {
			System.out.println(Thread.currentThread().getName() + " performing task2");
		};

		Runnable task3 = () -> {
			System.out.println(Thread.currentThread().getName() + " performing task3");
		};

		CustomThreadPool threadpool = new CustomThreadPool(4, 4);
		threadpool.execute(task1);
		Thread.sleep(1000);
		threadpool.execute(task2);
		Thread.sleep(1000);
		threadpool.execute(task3);

		// blocking queue property where thread will wait if queue is empty until another thread put something in queueu
		// and implicitly notify the thread which is waiting 
	}

}

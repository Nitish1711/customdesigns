/**
 * 
 */
package custom_thread_pool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author nitkulsh
 *
 */
public class CustomThreadPool {

	private BlockingQueue<Runnable> taskQueue = null;
	private List<PoolThread> poolThreads = new ArrayList<PoolThread>();

	public CustomThreadPool(int noOfThreads, int noOfTasks) {
		taskQueue = new ArrayBlockingQueue<Runnable>(noOfTasks);
		for (int i = 0; i < noOfThreads; i++) {
			poolThreads.add(new PoolThread(taskQueue));
		}
		for (int j = 0; j < poolThreads.size(); j++) {
			poolThreads.get(j).start();
		}

	}

	public synchronized void execute(Runnable task) throws InterruptedException {
		taskQueue.put(task);
	}

}

/**
 * 
 */
package custom_thread_pool;

import java.util.concurrent.BlockingQueue;

/**
 * @author nitkulsh
 *
 */
public class PoolThread extends Thread {

	private BlockingQueue<Runnable> taskQueue;

	public PoolThread(BlockingQueue<Runnable> taskQueue) {
		this.taskQueue = taskQueue;
	}

	public void run() {
		try {
			Runnable runnable = taskQueue.take();
			runnable.run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

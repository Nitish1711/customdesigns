package com.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueImpl {
	
	public static void main(String[] args) {
		
		Queue<Integer> queue=new LinkedList<Integer>();
		
		for(int i=0;i<5;i++) {
			queue.add(i);
		}
		
		Integer head=queue.peek();
		System.out.println("head" + head);
		
		System.out.println("queue elements ");

		System.out.print(queue);
		
		Integer headRemoved=queue.poll();
		System.out.println(headRemoved);

		System.out.println(queue);
		
		Integer headRemoveds=queue.remove();
		
		System.out.println(queue);
	
	}

}

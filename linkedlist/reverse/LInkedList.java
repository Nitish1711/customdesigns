/**
 * 
 */
package com.linkedlist.reverse;

/**
 * @author Dell
 *
 */
public class LInkedList {

	Node head;

	public void push(int data) {
		Node newNode = new Node(data);
		newNode.next = head;
		head = newNode;
	}
	
	public void reverseList() {
		Node curr=head;
		Node prev=null;
		Node next=null;
		while(curr!=null) {
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		}
		
		while (prev!=null) {
			System.out.println(prev.data);
			prev=prev.next;
			
		}
	}
	
	public static void main(String[] args) {
        LInkedList llist = new LInkedList(); 
        
        llist.push(4); 
        llist.push(3); 
        llist.push(2); 
        llist.push(1); 
        
        llist.reverseList();

	}
}

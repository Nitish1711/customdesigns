/**
 * 
 */
package com.linkedlist.mergelinkedlist;

/**
 * @author Dell
 *
 */

//merge two sorted linked lists 
public class LInkedList {

	Node head;

	public void push(int data) {
		Node newNode = new Node(data);
		newNode.next = head;
		head = newNode;
	}

	public void reverseList() {
		Node curr = head;
		Node prev = null;
		Node next = null;
		while (curr != null) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		while (prev != null) {
			System.out.println(prev.data);
			prev = prev.next;

		}
	}

	public static void main(String[] args) {
		LInkedList llist = new LInkedList();
		LInkedList llist1 = new LInkedList();

		llist.push(12);
		llist.push(11);
		llist.push(7);
		llist.push(5);

		llist1.push(10);
		llist1.push(3);
		llist1.push(2);
		llist1.push(1);

		llist.sortedMerge(llist.head, llist1.head);
	}

	private void sortedMerge(Node head1, Node head2) {
		Node curr1 = head1;
		Node curr2 = head2;
		Node temp = null;

		while (curr1 != null && curr2 != null) {

			if (curr1.data <= curr2.data) {
				Node next1 = curr1.next;
				curr1.next = null;

				if (temp == null)
					temp = curr2;

				else {

					Node last = temp;
					while (last.next != null)
						last = last.next;

					last.next = curr1;
				}
				curr1 = next1;

			} else {
				Node next2 = curr2.next;
				curr2.next = null;

				if (temp == null)
					temp = curr2;

				else {
					Node last = temp;
					while (last.next != null)
						last = last.next;

					last.next = curr2;
				}
				curr2 = next2;
			}

		}

		while (temp != null) {
			System.out.println(temp.data);
			temp = temp.next;
		}

	}

}

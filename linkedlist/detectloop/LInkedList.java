package com.linkedlist.detectloop;

public class LInkedList {

	Node head;
	
	public boolean detectLoop() {
		Node singlePtr = head;
		Node doubptr = head;

		while (singlePtr != null && doubptr != null && doubptr.next != null) {

			singlePtr = singlePtr.next;
			doubptr = doubptr.next.next;

			if (singlePtr == doubptr) {
				return true;
			}

		}

		return false;

	}

	public void push(int data) {
		Node newNode = new Node(data);
		newNode.next = head;
		head = newNode;

	}


}

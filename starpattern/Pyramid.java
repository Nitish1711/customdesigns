package com.starpattern;

public class Pyramid {
	
	public static void main(String[] args) {
		
	//	printPyramid(5);
		printReversePyramid(5);

		
	}

	private static void printPyramid(int rows) {
		
	for(int i=1;i<=rows;i++) {
		int k=0;
		for(int j=1;j<=rows-i;j++) {
			System.out.print(" ");
		}
		while(k<2*i-1) {
			System.out.print("* ");
			k++;
		}
		System.out.println("");
	}
		
		
	}
	
	private static void printReversePyramid(int rows) {
		
	for(int i=rows;i>=1;i--) {
		int k=0;
		for(int j=1;j<=rows-i;j++) {
			System.out.print(" ");
		}
		while(k<2*i-1) {
			System.out.print("* ");
			k++;
		}
		System.out.println("");
	}
		
		
	}

}
